import React, { Component } from 'react';
import './App.css';
import FetchPersons from "./components/FetchPersons";

class App extends Component {
  state = {
    visible: true
  };

  render() {
    return (
      <div className="App">
        <FetchPersons />
      </div>
    )
  }
}

export default App;
